# Aka ATM

## DEMO

Click [here](https://atm-app.vercel.app/) to view this app's demo

## Technologies used

- ReactJS
- Bootstrap
- Antd
- Sass

## How to set up this app on your local invironment

These instructions will get you a copy of this project up and running on your local machine.

## Prerequisites

To install this project on your local machine, you need first to clone the repository `https://gitlab.com/patricktunez123/atm-app.git` or download the zip file and once this is set up you're going to need NODEJS installed on your machine.

## Installing

The installation of this application is straightforward, After cloning this repository to your local machine, cd into it using your terminal and run the following command

- yarn

It will install all the node modules for the project.

## Available scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

# Author

Patrick TUNEZERWANE, +250781429268

- [Linkedin](https://www.linkedin.com/in/patrick-tunezerwane-0a901ba8/)
- [Twitter](https://twitter.com/tunezpatrick)

---

## License

MIT License

Copyright (c) 2021 Patrick TUNEZERWANE

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
