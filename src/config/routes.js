export const routes = {
  Login: {
    name: "Login",
    url: "/",
  },
  Customer: {
    name: "Home",
    url: "/customer",
  },
  Balance: {
    name: "Balance",
    url: "/balance",
  },
  Deposit: {
    name: "Deposit",
    url: "/deposit",
  },
  Withdraw: {
    name: "Withdraw",
    url: "/withdraw",
  },
  TransferMoney: {
    name: "Transfermoney",
    url: "/transfer-money",
  },
};
