import { AiOutlineSearch, AiOutlineHome } from "react-icons/ai";
import { routes } from "../../config/routes";

export const MobileMenus = [
  {
    id: 1,
    title: "Home",
    url: routes.Customer.url,
    icon: <AiOutlineHome />,
  },
  {
    id: 2,
    title: routes.Balance.name,
    url: routes.Balance.url,
    icon: <AiOutlineSearch />,
  },
  {
    id: 3,
    title: routes.Deposit.name,
    url: routes.Deposit.url,
    icon: <AiOutlineSearch />,
  },
  {
    id: 4,
    title: routes.TransferMoney.name,
    url: routes.TransferMoney.url,
    icon: <AiOutlineSearch />,
  },
  {
    id: 5,
    title: routes.Withdraw.name,
    url: routes.Withdraw.url,
    icon: <AiOutlineSearch />,
  },
];
