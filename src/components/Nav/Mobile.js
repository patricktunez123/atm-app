import React from "react";
import { Link } from "react-router-dom";
import { MobileMenus } from "./MobileMenus";
import "./Nav.scss";

const Mobile = () => {
  return (
    <div className="mobile__menu">
      {MobileMenus?.map((menu) => (
        <Link key={menu.id} to={menu.url}>
          <div className="menu__items">
            <div className="icon">{menu.icon}</div>
            <span className="text-muted">{menu.title}</span>
          </div>
        </Link>
      ))}
    </div>
  );
};

export default Mobile;
