import React from "react";
import jsPDF from "jspdf";
import { useHistory } from "react-router-dom";
import { Button, message, Popconfirm } from "antd";
import moment from "moment";
import { numberWithCommas } from "../../helpers/numbersFormatter";
import { routes } from "../../config/routes";
import "./Nav.scss";

const Nav = () => {
  const history = useHistory();
  const userCard = localStorage.getItem("card");
  let balance = localStorage.getItem("balance");
  let name = localStorage.getItem("name");

  const generateReport = () => {
    let doc = new jsPDF();
    doc.text(20, 20, `Hello ${name && name},`);
    doc.text(
      20,
      30,
      `Your account report for card number : ${userCard && userCard} `
    );
    doc.text(20, 40, `From ${moment(new Date())}`);
    doc.text(
      20,
      50,
      `The account balance is ${balance && numberWithCommas(balance)}`
    );
    doc.save("Report.pdf");
  };

  const confirm = () => {
    message.success("Report is printed successfully");
    generateReport();
    localStorage.removeItem("card");
    localStorage.removeItem("pin");
    localStorage.removeItem("balance");
    history.push(routes.Login.url);
  };

  const cancel = () => {
    message.success("Successfully logged out");
    localStorage.removeItem("card");
    localStorage.removeItem("pin");
    history.push(routes.Login.url);
  };

  return (
    <>
      <div className="aka__atm__nav">
        <Popconfirm
          title="Do you want session report and receipt?"
          onConfirm={confirm}
          onCancel={cancel}
          okText="Yes please"
          cancelText="Nope"
        >
          <Button className="aka__atm--btn" type="primary">
            Logout
          </Button>
        </Popconfirm>
      </div>
    </>
  );
};

export default Nav;
