import React from "react";
import Nav from "../Nav";
import Mobile from "../Nav/Mobile";
import Sidebar from "../Sidebar";
import "./Layout.scss";

const Layout = ({ children }) => {
  return (
    <div className="aka__atm__app">
      <Sidebar />
      <div className="aka__atm__container">
        <Nav />
        <div className="container-fluid">{children}</div>
      </div>
      <Mobile />
    </div>
  );
};

export default Layout;
