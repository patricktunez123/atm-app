import React from "react";
import { Form, Input, Button, Popconfirm } from "antd";
import { numberWithCommas } from "../../helpers/numbersFormatter";

const DepositCashForm = ({ confirm, cancel, handleChange, depositAmount }) => {
  return (
    <Form>
      <div className="row aka--atm--row">
        <div className="col-lg-12 col-md-12 col-12">
          <Form.Item
            name="amount"
            rules={[
              {
                required: true,
                pattern: new RegExp(/^[1-9]+[0-9]*$/),
                message: "Enter valid number",
              },
            ]}
          >
            <Input
              className="aka__atm--input"
              size="large"
              placeholder="Amount"
              onChange={(e) => handleChange(e.target.value)}
            />
          </Form.Item>
        </div>

        <div className="col-lg-4 col-md-4 col-12">
          <Form.Item>
            <Popconfirm
              title={`Do you want to deposit ${numberWithCommas(
                depositAmount
              )} RWF?`}
              onConfirm={confirm}
              onCancel={cancel}
              okText="Yes"
              cancelText="Cancel"
            >
              <Button
                className="aka__atm--btn"
                type="primary"
                htmlType="submit"
              >
                Deposit
              </Button>
            </Popconfirm>
          </Form.Item>
        </div>
      </div>
    </Form>
  );
};

export default DepositCashForm;
