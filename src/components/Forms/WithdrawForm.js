import React from "react";
import { Form, Input, Button } from "antd";

const WithdrawForm = ({ onFinish }) => {
  return (
    <Form name="basic" onFinish={onFinish}>
      <div className="row">
        <div className="col-lg-12 col-md-12 col-12">
          <Form.Item
            name="amount"
            rules={[
              {
                required: true,
                pattern: new RegExp(/^[1-9]+[0-9]*$/),
                message: "Enter valid number",
              },
            ]}
          >
            <Input
              className="aka__atm--input"
              size="large"
              placeholder="Amount"
            />
          </Form.Item>
        </div>

        <div className="col-lg-4 col-md-4 col-12">
          <Form.Item>
            <Button className="aka__atm--btn" type="primary" htmlType="submit">
              Withdraw
            </Button>
          </Form.Item>
        </div>
      </div>
    </Form>
  );
};

export default WithdrawForm;
