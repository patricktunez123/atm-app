import React from "react";
import { Form, Input, Button } from "antd";

const TransferForm = ({ onFinish }) => {
  return (
    <Form name="basic" onFinish={onFinish}>
      <div className="row">
        <div className="col-lg-12 col-md-12 col-12">
          <Form.Item
            name="amount"
            rules={[
              {
                required: true,
                pattern: new RegExp(/^[1-9]+[0-9]*$/),
                message: "Enter valid number",
              },
            ]}
          >
            <Input
              className="aka__atm--input"
              size="large"
              placeholder="Amount"
            />
          </Form.Item>
        </div>
        <div className="col-lg-12 col-md-12 col-12">
          <Form.Item
            name="accountNumber"
            rules={[
              {
                required: true,
                pattern: new RegExp(
                  /^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/
                ),
                message: "That card is not valid",
              },
            ]}
          >
            <Input
              className="aka__atm--input"
              size="large"
              placeholder="Account number"
            />
          </Form.Item>
        </div>

        <div className="col-lg-4 col-md-4 col-12">
          <Form.Item>
            <Button className="aka__atm--btn" type="primary" htmlType="submit">
              Transfer
            </Button>
          </Form.Item>
        </div>
      </div>
    </Form>
  );
};

export default TransferForm;
