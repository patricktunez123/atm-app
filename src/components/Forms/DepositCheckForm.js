import React from "react";
import { Form, Upload, Button, Popconfirm, message } from "antd";
import { InboxOutlined } from "@ant-design/icons";
import { numberWithCommas } from "../../helpers/numbersFormatter";

const { Dragger } = Upload;

const DepositCheckForm = ({
  onFinish,
  confirm,
  cancel,
  checkDepositAmount,
}) => {
  const props = {
    name: "file",
    multiple: true,
    action: "https://www.mocky.io/v2/5cc8019d300000980a055e76",
    onChange(info) {
      const { status } = info.file;
      if (status !== "uploading") {
        console.log(info.file, info.fileList);
      }
      if (status === "done") {
        message.success(`${info.file.name} check uploaded successfully.`);
      } else if (status === "error") {
        message.error(`${info.file.name} check upload failed.`);
      }
    },
    onDrop(e) {
      console.log("Dropped files", e.dataTransfer.files);
    },
  };

  return (
    <Form name="basic" onFinish={onFinish}>
      <div className="row aka--atm--row">
        <div className="col-lg-12 col-md-12 col-12 ">
          <Form.Item
            name="amount"
            rules={[
              {
                required: true,
                message: "No check entered",
              },
            ]}
          >
            <Dragger {...props}>
              <p className="ant-upload-drag-icon">
                <InboxOutlined />
              </p>
              <p className="ant-upload-text">Click here to enter the check</p>
            </Dragger>
          </Form.Item>
        </div>

        <div className="col-lg-4 col-md-4 col-12">
          <Form.Item>
            <Popconfirm
              title={`Do you want to deposit ${numberWithCommas(
                checkDepositAmount
              )}?`}
              onConfirm={confirm}
              onCancel={cancel}
              okText="Yes"
              cancelText="Cancel"
            >
              <Button
                className="aka__atm--btn"
                type="primary"
                htmlType="submit"
              >
                Deposit
              </Button>
            </Popconfirm>
          </Form.Item>
        </div>
      </div>
    </Form>
  );
};

export default DepositCheckForm;
