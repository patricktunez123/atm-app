import { FaMoneyCheck } from "react-icons/fa";
import { GiTakeMyMoney } from "react-icons/gi";
import { BiTransfer } from "react-icons/bi";
import { FaMoneyCheckAlt } from "react-icons/fa";
import { FiHome } from "react-icons/fi";
import { routes } from "../../config/routes";

export const Menus = [
  {
    id: 1,
    title: routes.Customer.name,
    url: routes.Customer.url,
    icon: <FiHome />,
  },
  {
    id: 2,
    title: routes.Balance.name,
    url: routes.Balance.url,
    icon: <FaMoneyCheck />,
  },
  {
    id: 3,
    title: routes.Deposit.name,
    url: routes.Deposit.url,
    icon: <GiTakeMyMoney />,
  },
  {
    id: 4,
    title: routes.TransferMoney.name,
    url: routes.TransferMoney.url,
    icon: <BiTransfer />,
  },
  {
    id: 5,
    title: routes.Withdraw.name,
    url: routes.Withdraw.url,
    icon: <FaMoneyCheckAlt />,
  },
];
