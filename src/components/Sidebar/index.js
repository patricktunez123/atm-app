import React from "react";
import { Link } from "react-router-dom";
import { Menus } from "./Menus";
import { routes } from "../../config/routes";
import logo from "../../images/logo.svg";
import "./Sidebar.scss";

const Sidebar = () => {
  return (
    <div className="aka__atm__sidebar">
      <div className="top">
        <Link to={routes.Customer.url}>
          <img className="logo" src={logo} alt="Logo" />
        </Link>
      </div>
      <div className="menus">
        {Menus?.map((menu) => (
          <Link key={menu.id} to={menu.url}>
            <div className="linkmenu">
              {menu.icon}
              <span>{menu.title}</span>
            </div>
          </Link>
        ))}
      </div>
      <div className="bottom ">
        <span className="small-text">Aka ATM Alrights resrved </span>
      </div>
    </div>
  );
};

export default Sidebar;
