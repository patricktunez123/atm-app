import React from "react";
import { numberWithCommas } from "../../helpers/numbersFormatter";

const Balance = () => {
  const balance = localStorage.getItem("balance");
  return <span>{`${balance && numberWithCommas(balance)} RWF`}</span>;
};

export default Balance;
