import React from "react";
import { Redirect } from "react-router-dom";
import { routes } from "../../config/routes";
import Userbalance from "../../components/Balance";
import savingsImage from "../../images/savings_tunez.svg";
import "./Balance.scss";

const Balance = () => {
  const userCard = localStorage.getItem("card");
  const userPin = localStorage.getItem("pin");
  return (
    <>
      {userCard && userPin ? (
        <div className="aka__atm__page">
          <div className="page-header">
            <img src={savingsImage} alt="Aka ATM 2" />
          </div>
          <h6 className="title">
            Your balance is{" "}
            <span className="primary-text font-900">
              <Userbalance />
            </span>
          </h6>
        </div>
      ) : (
        <Redirect to={routes.Login.url} />
      )}
    </>
  );
};

export default Balance;
