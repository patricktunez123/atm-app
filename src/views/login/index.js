import React, { useState } from "react";
import Cards from "react-credit-cards";
import { SmileOutlined } from "@ant-design/icons";
import { message, notification } from "antd";
import "react-credit-cards/es/styles-compiled.css";
import { Redirect, useHistory } from "react-router-dom";
import { routes } from "../../config/routes";
import "./Login.scss";

const Login = () => {
  const history = useHistory();
  const initialState = 0;
  const [number, setNumber] = useState("");
  const [name, setName] = useState("");
  const [cvc, setCvc] = useState("");
  const [focus, setFocus] = useState("");
  const [count, setCount] = useState(initialState);

  const userCard = localStorage.getItem("card");
  const userPin = localStorage.getItem("pin");

  const creditCardValidation = (e, creditCradNum, userPin) => {
    e.preventDefault();
    const pinregEx = /^\d{4}$/;
    const regEx =
      /^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/;

    if (creditCradNum && creditCradNum.match(regEx)) {
      console.log("that was a valid card");
      localStorage.setItem("card", creditCradNum);
      localStorage.setItem("balance", 350000);
      localStorage.setItem("name", name);
    } else {
      message.error("Use a valid real card");
    }
    if (userPin && userPin.match(pinregEx)) {
      localStorage.setItem("pin", userPin);
      setCount(initialState);
    } else {
      setCount((prevState) => prevState + 1);
      message.error("PIN must be 4 numbers");
    }
  };

  if (count >= 3) {
    notification.open({
      message: "Notice from Aka ATM",
      description:
        "Due to using wrong PN for many times we have temporary locked your card. Please call us on  +25078 000 00 00.",
      icon: <SmileOutlined style={{ color: "#108ee9" }} />,
    });
    setCount(initialState);
  }

  if (userCard && userPin) {
    history.push(routes.Customer.url);
  }
  return (
    <div className="aka__atm__login">
      {userCard && userPin ? (
        <Redirect to={routes.Customer.url} />
      ) : (
        <div className="login--container">
          <Cards
            cvc={cvc}
            expiry="09/2023"
            focused={focus}
            name={name}
            number={number}
          />
          <form className="login--form">
            <input
              type="tel"
              name="number"
              placeholder="Enter card Number"
              value={number}
              onChange={(e) => setNumber(e.target.value)}
              onFocus={(e) => setFocus(e.target.name)}
              maxLength={16}
              className="aka__atm--input second--input"
            />
            <input
              type="text"
              name="name"
              placeholder="Name on the card"
              value={name}
              onChange={(e) => setName(e.target.value)}
              onFocus={(e) => setFocus(e.target.name)}
              className="aka__atm--input second--input"
            />
            <input
              type="tel"
              name="cvc"
              placeholder="PIN"
              maxLength={4}
              value={cvc}
              onChange={(e) => setCvc(e.target.value)}
              onFocus={(e) => setFocus(e.target.name)}
              className="aka__atm--input second--input"
            />
            <button
              className="aka__atm--btn"
              onClick={(e) => creditCardValidation(e, number, cvc)}
            >
              Login
            </button>
          </form>
        </div>
      )}
    </div>
  );
};

export default Login;
