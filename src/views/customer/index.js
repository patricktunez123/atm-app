import React from "react";
import { Redirect } from "react-router-dom";
import Balance from "../../components/Balance";
import { routes } from "../../config/routes";
import cardImage from "../../images/credit_card_tunez.svg";
import "./Customer.scss";

const Customer = () => {
  const userCard = localStorage.getItem("card");
  const userPin = localStorage.getItem("pin");
  const name = localStorage.getItem("name");

  return (
    <>
      {userCard && userPin ? (
        <div className="aka__atm__page">
          <div className="page-header">
            <img src={cardImage} alt="Aka ATM 1" />
          </div>
          <h6 className="title">Welcome {name && name}!</h6>
          <h6 className="font-500">
            You have{" "}
            <span className="primary-text font-900">
              <Balance />
            </span>
          </h6>
        </div>
      ) : (
        <Redirect to={routes.Login.url} />
      )}
    </>
  );
};

export default Customer;
