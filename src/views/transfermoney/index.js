import React from "react";
import { Redirect } from "react-router";
import { message } from "antd";
import TransferForm from "../../components/Forms/TransferForm";
import { numberWithCommas } from "../../helpers/numbersFormatter";
import transImage from "../../images/trans.svg";
import { routes } from "../../config/routes";
import "./TransferMoney.scss";

const Transfer = () => {
  const userCard = localStorage.getItem("card");
  const userPin = localStorage.getItem("pin");
  let balance = localStorage.getItem("balance");

  const onFinish = (values) => {
    if (parseInt(values?.amount) > balance) {
      message.error(
        `You have no ${numberWithCommas(
          values?.amount
        )}, Your balance is ${numberWithCommas(balance)}. Please try again`
      );
    } else {
      message.success("Transaction is complete successfully");
      balance = parseInt(balance) - parseInt(values?.amount);
      localStorage.setItem("balance", balance);
      message.info(`Your new balance is ${numberWithCommas(balance)}`);
    }
  };

  return (
    <>
      {userCard && userPin ? (
        <div className="aka__atm__page">
          <div className="page-header">
            <img src={transImage} alt="Aka ATM 3" />
          </div>
          <TransferForm onFinish={onFinish} />
        </div>
      ) : (
        <Redirect to={routes.Login.url} />
      )}
    </>
  );
};

export default Transfer;
