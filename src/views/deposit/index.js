import React, { useState } from "react";
import { message } from "antd";
import DepositCashForm from "../../components/Forms/DepositCashForm";
import DepositCheckForm from "../../components/Forms/DepositCheckForm";
import { numberWithCommas } from "../../helpers/numbersFormatter";
import { routes } from "../../config/routes";
import "./Deposit.scss";
import { Redirect } from "react-router";

const Deposit = () => {
  const initialState = 0;
  const [depositAmount, setDepositAmount] = useState(initialState);
  const userCard = localStorage.getItem("card");
  const userPin = localStorage.getItem("pin");
  let balance = localStorage.getItem("balance");
  const checkDepositAmount = 5000;

  const handleChange = (amount) => {
    setDepositAmount(amount);
  };

  const onFinish = (values) => {
    console.log(values);
  };

  function confirm() {
    message.success("Your balance is updated successfully");
    balance = parseInt(balance) + parseInt(depositAmount);
    localStorage.setItem("balance", balance);
    message.info(`Your new balance is ${numberWithCommas(balance)}`);
  }

  function confirmCheck() {
    message.success("Your balance is updated successfully");
    balance = parseInt(balance) + checkDepositAmount;
    localStorage.setItem("balance", balance);
    message.info(`Your new balance is ${numberWithCommas(balance)}`);
  }

  function cancel() {
    message.error("Ejected");
  }

  return (
    <>
      {userCard && userPin ? (
        <div className="aka__atm__page">
          <div className="row">
            <div className="col-lg-6 col-md-6 col-12">
              <h6 className="title">Deposit cash</h6>
              <DepositCashForm
                confirm={confirm}
                cancel={cancel}
                handleChange={handleChange}
                depositAmount={depositAmount}
              />
            </div>
            <div className="col-lg-6 col-md-6 col-12">
              <h6 className="title">Deposit check</h6>
              <DepositCheckForm
                onFinish={onFinish}
                confirm={confirmCheck}
                cancel={cancel}
                checkDepositAmount={checkDepositAmount}
              />
            </div>
          </div>
        </div>
      ) : (
        <Redirect to={routes.Login.url} />
      )}
    </>
  );
};

export default Deposit;
