import React from "react";
import { Redirect } from "react-router";
import { message } from "antd";
import WithdrawForm from "../../components/Forms/WithdrawForm";
import { numberWithCommas } from "../../helpers/numbersFormatter";
import withdraw from "../../images/withdraw.svg";
import { routes } from "../../config/routes";
import "./Withdraw.scss";

const Withdraw = () => {
  const userCard = localStorage.getItem("card");
  const userPin = localStorage.getItem("pin");
  let balance = localStorage.getItem("balance");

  const onFinish = (values) => {
    if (parseInt(values?.amount) > balance) {
      message.error(
        `You have no ${numberWithCommas(
          values?.amount
        )}, Your balance is ${numberWithCommas(balance)}. Please try again`
      );
    } else {
      message.success("Withdraw is complete");
      balance = parseInt(balance) - parseInt(values?.amount);
      localStorage.setItem("balance", balance);
      message.info(`Your new balance is ${numberWithCommas(balance)}`);
    }
  };

  return (
    <>
      {userCard && userPin ? (
        <div className="aka__atm__page">
          <div className="page-header">
            <img src={withdraw} alt="Aka ATM 4" />
          </div>
          <WithdrawForm onFinish={onFinish} />
        </div>
      ) : (
        <Redirect to={routes.Login.url} />
      )}
    </>
  );
};

export default Withdraw;
