import React, { Suspense, Fragment } from "react";
import { Switch, Route } from "react-router-dom";
import { Spin } from "antd";
import Layout from "./components/Layout";
import { routes } from "./config/routes";
import Login from "./views/login";

const Customer = React.lazy(() => import("./views/customer"));
const Balance = React.lazy(() => import("./views/balance"));
const Deposit = React.lazy(() => import("./views/deposit"));
const Transfermoney = React.lazy(() => import("./views/transfermoney"));
const Withdraw = React.lazy(() => import("./views/withdraw"));

function App() {
  const userCard = localStorage.getItem("card");
  const userPin = localStorage.getItem("pin");

  return (
    <Fragment>
      <Suspense
        fallback={
          <>
            {!userCard && !userPin ? (
              <div
                style={{
                  height: "100vh",
                  width: "100%",
                  textAlign: "center",
                  paddingTop: "calc(50vh - 7px)",
                }}
              >
                <Spin />
              </div>
            ) : (
              <Layout>
                <div
                  style={{
                    height: "100vh",
                    width: "100%",
                    textAlign: "center",
                    paddingTop: "calc(50vh - 7px)",
                  }}
                >
                  <Spin />
                </div>
              </Layout>
            )}
          </>
        }
      >
        <Switch>
          <Route path={routes.Login.url} exact>
            <Login />
          </Route>
          <Route path={routes.Customer.url} exact>
            <Layout>
              <Customer />
            </Layout>
          </Route>
          <Route path={routes.Balance.url} exact>
            <Layout>
              <Balance />
            </Layout>
          </Route>
          <Route path={routes.Deposit.url} exact>
            <Layout>
              <Deposit />
            </Layout>
          </Route>
          <Route path={routes.TransferMoney.url} exact>
            <Layout>
              <Transfermoney />
            </Layout>
          </Route>
          <Route path={routes.Withdraw.url} exact>
            <Layout>
              <Withdraw />
            </Layout>
          </Route>
          <Route>
            <Layout>
              <h1>Not found</h1>
            </Layout>
          </Route>
        </Switch>
      </Suspense>
    </Fragment>
  );
}

export default App;
